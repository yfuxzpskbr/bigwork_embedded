#include "music.h"
#include "delay.h"
int High,Low;
bit isClock = 0;
unsigned char code FREQH[28]={
                         0xF2,0xF3,0xF5,0xF5,0xF6,0xF7,0xF8, //C3
                         0xF9,0xF9,0xFA,0xFA,0xFB,0xFB,0xFC, //中央C4
						 0xFC,0xFC,0xFD,0xFD,0xFD,0xFD,0xFE, //C5
                         0xFE,0xFE,0xFE,0xFE,0xFE,0xFE,0xFF, //C6
						 };
// 音阶频率表 低八位
unsigned char code FREQL[28]={
                         0x42,0xC1,0x17,0xB6,0xD0,0xD1,0xB6, //C3 --> -1
                         0x21,0xE1,0x8C,0xD8,0x68,0xE9,0x5B, //中央C4  --> +6
						 0x8F,0xEE,0x44,0x6B,0xB4,0xF4,0x2D, //C5   --> +13
                         0x47,0x77,0xA2,0xB6,0xDA,0xFA,0x16, //C6	--> +20
                         };

unsigned char code MUSIC[16][16]={
			{8,12,2,16,2,16,2,16,2},
			{4,16,6,15,2},
			{8,14,3,15,1,14,2,13,2},
			{2,12,8},
			{8,19,2,19,2,19,2,19,2},
			{4,19,6,18,2},
			{8,16,2,18,2,18,2,17,2},
			{2,16,8},
			{8,16,2,19,2,19,2,18,2},
			{4,16,6,15,2},
			{8,14,3,15,1,14,2,13,2},
			{4,12,4,9,4},
			{8,9,2,14,2,14,2,13,2},
			{4,12,6,16,2},
			{8,15,3,14,1,13,2,11,2},
			{2,12,8}
};
void playSong(unsigned char * music,int row){
	int i,j,len,t=150;
	for(i=0;i<row&&isClock;i++){
		if(isClock==0){return;}
		j=0;
		len=music[i*16];
		j++;
		while(j<=len&&isClock){
			if(isClock==0){return;}
			if(isClock){
				High = FREQH[music[i*16+j]];
   				Low = FREQL[music[i*16+j]];
				TR1=1;
				j++;
				delay(t*music[i*16+j]);
				TR1=0;
				j++;
			}
		}
	}
}

void Init_Timer1()
{
	 TMOD |= 0x10;	  //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响		     
	 EA=1;            //总中断打开
	 ET1=1;           //定时器中断打开
}

void Timer1_ISR() interrupt 3 
{
	 TH1=High;
	 TL1=Low;
	 SPK=~SPK;
}