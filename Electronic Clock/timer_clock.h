#ifndef TIMER_CLOCK_H
#define TIMER_CLOCK_H
#include <reg51.h>
#define uchar unsigned char
#define WEEK 7
#define SECOND 6
#define MINUTE 5
#define HOUR 4
#define DAY 3
#define MONTH 2
#define YEAR 1

sbit INC = P2^6;  //增加按钮接口
sbit DEC = P2^7;  //减少按钮接口
extern uchar second,min,hour;	 //时分秒记录变量
extern uchar secondC,minC,hourC;	//闹钟记录变量
extern uchar _week_,month,day;   //年月日记录变量
extern unsigned int year;
extern uchar day_of_month[12];	 //每月的天数
//LCD缓冲区
extern char line1[];
extern char line2[];

extern char code week[][4];	  //星期的单词数组
extern uchar set_mode; //时间设置模式
extern uchar set_clock_mode;  //闹钟设置模式
extern uchar count;	  //计数值

void init_INT();
void init_timer();
void show_Time();
void show_year();
void set_year();
void show_month();
void set_month();
void show_day();
void set_day();
void show_week();
void set_week();
void show_hour();
void set_hour();
void show_minute();
void set_minute();
void show_second();
void set_second();
void show_secondC();
void set_secondC();
void show_minuteC();
void set_minuteC();
void show_hourC();
void set_hourC();
void second_add();
void minute_add();
void hour_add();
void day_add();
void month_add();
bit isLeapYear(uchar year);

#endif