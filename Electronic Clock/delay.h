#ifndef DELAY_H
#define DELAY_H

#define uchar unsigned char
#define uint unsigned int
#include <intrins.h>
void delay(int t);
void delay_ms(uint t);
void delay_10us();
void delay_600us();
void delay_us(uchar t);

#endif