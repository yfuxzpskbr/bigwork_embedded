#ifndef DS18B20_H
#define DS18B20_H

#include <reg51.h>
#define uchar unsigned char
#define uint unsigned int

sbit DQ=P3^7;
extern bit first;
extern int temperature;

bit DS18B20_init();
void write_byte_to_DS18B20(uchar data_);
uchar read_byte_from_DS18B20();
void get_temperature();

#endif