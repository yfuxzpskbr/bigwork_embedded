#ifndef LCD1602_H
#define LCD1602_H

#include <reg51.h>
#define LCD_Buffer P0
#define uchar unsigned char
#define uint unsigned int
sbit RS=P2^3;
sbit RW=P2^4;
sbit E=P2^5;

void LCD_Init();
void write_command_to_LCD(uchar data_);
void write_data_to_LCD(uchar data_);
void write_str_to_LCD(uchar sx,uchar sy,char *ptr);

#endif