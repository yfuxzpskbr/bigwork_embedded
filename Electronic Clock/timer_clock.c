#include "timer_clock.h"
#include "LCD1602.h"
#include "DS18B20.h"
#include "delay.h"
#include "music.h"
#include <reg51.h>
uchar second=30,min=57,hour=23;
uchar secondC=60,minC=60,hourC=24;
uchar _week_=6,month=7,day=10;
uchar day_of_month[12]={31,28,31,30,31,30,31,31,30,31,30,31};
char line1[]="2021-07-06    ";
char line2[]="22:22:22   MON  ";
unsigned int year=2021;
char code week[][4]={"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
uchar set_mode=0;
uchar set_clock_mode = 0;
uchar count=0;

void init_INT(){
	EA = 1;
	EX0 = 1;
	EX1 = 1;
	IT0 = 0;
	IT1 = 0;
}

void init_timer(){
	TMOD = 0x01 | TMOD;
	TL0 = (65536-50000)%256;
	TH0 = (65536-50000)/256;
	EA = 1;
	ET0 = 1;
	PT0=1;
}

void timer0_ISR() interrupt 1{
	TL0 = (65536-50000)%256;
	TH0 = (65536-50000)/256;
	count++;
	if(count%20==0){
		if(set_mode==0){
		second_add();  }
		if(count==20){
			count=0;
			EX1=1;
			EX0=1;
		}
	}
}

void show_Time(){
	line1[0]=year/1000+'0';
	line1[1]=year/100%10+'0';
	line1[2]=year/10%10+'0';
	line1[3]=year%10+'0';
	line1[5]=month/10+'0';
	line1[6]=month%10+'0';
	line1[8]=day/10+'0';
	line1[9]=day%10+'0';
	line1[11]=' ';
	if(temperature<0){
		line1[11] = '-';
		temperature = -temperature;
	}
	line1[12]=temperature/10+'0';
	line1[13]=temperature%10+'0';
	if(line1[12]=='0'){
		line1[12]=line1[11];
		line1[11]=' ';
	}
	write_str_to_LCD(0,0,line1);
	write_command_to_LCD(0x80|0x0E);
	write_data_to_LCD(0xDF);
	write_command_to_LCD(0x80|0x0F);
	write_data_to_LCD('C');
	line2[0]=hour/10+'0';
	line2[1]=hour%10+'0';
	line2[3]=min/10+'0';
	line2[4]=min%10+'0';
	line2[6]=second/10+'0';
	line2[7]=second%10+'0';
	line2[11]=week[_week_][0];
	line2[12]=week[_week_][1];
	line2[13]=week[_week_][2];
	if(set_clock_mode==0){
	write_str_to_LCD(1,0,line2);}
}
void show_year(){
	line1[0]=year/1000+'0';
	line1[1]=year/100%10+'0';
	line1[2]=year/10%10+'0';
	line1[3]=year%10+'0';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
	line1[0]=' ';
	line1[1]=' ';
	line1[2]=' ';
	line1[3]=' ';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
}
void set_year(){
	int i;
	while(set_mode==YEAR){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==YEAR){//等待按键按下
			show_year();	
		}
		while(INC==0&&set_mode==YEAR){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==YEAR){//等待按键抬起
			i = -1;
		}
		year=year+i;			
	}
}
void show_month(){
	line1[5]=month/10+'0';
	line1[6]=month%10+'0';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
	line1[5]=' ';
	line1[6]=' ';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
}
void set_month(){
	int i;
	while(set_mode==MONTH){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==MONTH){//等待按键按下
			show_month();	
		}
		while(INC==0&&set_mode==MONTH){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==MONTH){//等待按键抬起
			i = -1;
		}
		month=month+i;
		if(month==13){
			month=1;
		}			
	}
}
void show_day(){
	line1[8]=day/10+'0';
	line1[9]=day%10+'0';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
	line1[8]=' ';
	line1[9]=' ';
	write_str_to_LCD(0,0,line1);
	delay_us(500);
}
void set_day(){
	int i;
	if(isLeapYear(year)){
		day_of_month[1]=29;
	}
	while(set_mode==DAY){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==DAY){//等待按键按下
			show_day();	
		}
		while(INC==0&&set_mode==DAY){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==DAY){//等待按键抬起
			i = -1;
		}
		day=day+i;
		if(day>day_of_month[month-1]){
			day=1;
		}
		if(day<=0){
			day=day_of_month[month-1];
		}			
	}
}
void show_week(){
	line2[11]=week[_week_][0];
	line2[12]=week[_week_][1];
	line2[13]=week[_week_][2];
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[11]=' ';
	line2[12]=' ';
	line2[13]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_week(){
	int i;
	while(set_mode==WEEK){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==WEEK){//等待按键按下
			show_week();	
		}
		while(INC==0&&set_mode==WEEK){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==WEEK){//等待按键抬起
			i = -1;
		}
		_week_ = (_week_+7+i)%7;			
	}
}
void show_hour(){
	line2[0]=hour/10+'0';
	line2[1]=hour%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[0]=' ';
	line2[1]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_hour(){
	int i;
	while(set_mode==HOUR){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==HOUR){//等待按键按下
			show_hour();	
		}
		while(INC==0&&set_mode==HOUR){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==HOUR){//等待按键抬起
			i = -1;
		}
		hour = (hour+24+i)%24;			
	}
}
void show_minute(){
	line2[3]=min/10+'0';
	line2[4]=min%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[3]=' ';
	line2[4]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_minute(){
	int i;
	while(set_mode==MINUTE){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==MINUTE){//等待按键按下
			show_minute();	
		}
		while(INC==0&&set_mode==MINUTE){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==MINUTE){//等待按键抬起
			i = -1;
		}
		min = (min+60+i)%60;			
	}
}
void show_second(){
	line2[6]=second/10+'0';
	line2[7]=second%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[6]=' ';
	line2[7]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_second(){
	int i;
	while(set_mode==SECOND){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_mode==SECOND){//等待按键按下
			show_second();	
		}
		while(INC==0&&set_mode==SECOND){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_mode==SECOND){//等待按键抬起
			i = -1;
		}
		second = (second+60+i)%60;			
	}
}
void show_secondC(){
	line2[0]=hourC/10+'0';
	line2[1]=hourC%10+'0';
	line2[3]=minC/10+'0';
	line2[4]=minC%10+'0';
	line2[6]=secondC/10+'0';
	line2[7]=secondC%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[6]=' ';
	line2[7]=' ';
	write_str_to_LCD(1,0,line2);
 	delay_us(500);
}
void set_secondC(){
	int i;
	while(set_clock_mode==3){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_clock_mode==3){//等待按键按下
			show_secondC();	
		}
		while(INC==0&&set_clock_mode==3){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_clock_mode==3){//等待按键抬起
			i = -1;
		}
		secondC = (secondC+60+i)%60;			
	}
}
void show_minuteC(){
	line2[0]=hourC/10+'0';
	line2[1]=hourC%10+'0';
	line2[3]=minC/10+'0';
	line2[4]=minC%10+'0';
	line2[6]=secondC/10+'0';
	line2[7]=secondC%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[3]=' ';
	line2[4]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_minuteC(){
	int i;
	while(set_clock_mode==2){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_clock_mode==2){//等待按键按下
			show_minuteC();	
		}
		while(INC==0&&set_clock_mode==2){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_clock_mode==2){//等待按键抬起
			i = -1;
		}
		minC = (minC+60+i)%60;			
	}
}
void show_hourC(){
	line2[0]=hourC/10+'0';
	line2[1]=hourC%10+'0';
	line2[3]=minC/10+'0';
	line2[4]=minC%10+'0';
	line2[6]=secondC/10+'0';
	line2[7]=secondC%10+'0';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
	line2[0]=' ';
	line2[1]=' ';
	write_str_to_LCD(1,0,line2);
	delay_us(500);
}
void set_hourC(){
	int i;
	secondC = 0;
	minC = min;
	hourC = hour;
	while(set_clock_mode==1){
		INC=1;
		DEC=1;
		i=0;
		show_Time();
		while(INC==1&&DEC==1&&set_clock_mode==1){//等待按键按下
			show_hourC();	
		}
		while(INC==0&&set_clock_mode==1){//等待按键抬起
			i = 1;
		}	
		while(DEC==0&&set_clock_mode==1){//等待按键抬起
			i = -1;
		}
		hourC = (hourC+24+i)%24;			
	}
}
void second_add(){
	second++;
	if(second==60){
		second=0;
		minute_add();
	}
}
void minute_add(){
	min++;
	if(min==60){
		min=0;
		hour_add();
	}
}
void hour_add(){
	hour++;
	if(hour==24){
		hour=0;
		day_add();
	}
}

void day_add(){
	day++;
	_week_=(_week_+1)%7;
	if(day==32&&(month==1 || month==3 || month==5 || month==7 ||
		month==8 || month==10 || month==12)){
			day=1;
			month_add();
		}
	if(day==31&&(month==4 || month==6 || month==9 || month==11)){
			day=1;
			month_add();
		}
	if(day==30&&month==2&&isLeapYear(year)){
			day=1;
			month_add();
		}
	if(day==29&&month==2&&(!isLeapYear(year))){
			day=1;
			month_add();
		}
}

void month_add(){
	month++;
	if(month==13){
		month=1;
		year++;
	}
}
bit isLeapYear(uchar year){
	return ((year%4==0)&&(year%100)!=0)||(year%400==0);
}
void setTime_ISR() interrupt 0 {
	EX0 = 0;
	delay_ms(100);
	if(P3^2==0){
		delay_ms(100);
		if(P3^2==0){
			delay_ms(100);
			if(P3^2==0){
				set_mode=(set_mode+1)%8;
			}
		}
	}
	delay_ms(250);
}

void set_Clock_ISR() interrupt 2{
	EX1=0;
	delay_ms(100);
	if(P3^3==0){
		delay_ms(100);
		if(P3^3==0){
			delay_ms(100);
			if(P3^3==0){
				if(isClock){
					isClock=0;
					TR1=0;
				}else{
					set_clock_mode = (set_clock_mode+1)%4;
				}
			}
		}
	}
	delay_ms(250);
}

