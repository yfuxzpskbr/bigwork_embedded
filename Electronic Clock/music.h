#ifndef MUSIC_H
#define MUSIC_H

#include <reg51.h>
#define uchar unsigned char
extern int High,Low;
sbit SPK = P3^6;
extern bit isClock;
extern uchar code FREQH[28];
extern uchar code FREQL[28];
extern uchar code MUSIC[16][16];
void playSong(unsigned char * music,int row);
void Init_Timer1();
#endif