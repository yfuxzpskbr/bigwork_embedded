#include "DS18B20.h"
#include "delay.h"

bit first=1;
int temperature=39;

bit DS18B20_init()//DS18B20初始化
{
	bit ack = 0;
	DQ=0;//主动拉低480-960us（此处选择600us）
	delay_600us();
	DQ=1;//释放总线,传感器15-60us后拉低总线
	//while(DQ);
	delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();
	ack = DQ;
	//while(!DQ);//度过传感器被拉低的时间（60-240us）后主动拉高
	delay_600us();
	DQ=1;//主动拉高
	return ack;
}
void write_byte_to_DS18B20(uchar data_)//从低位开始写入
{
	unsigned char mask;
	for(mask=0x01;mask!=0;mask<<=1)
	{
		DQ=0;
		_nop_();_nop_();_nop_();_nop_();_nop_();//先拉低5us
		DQ = data_&mask;
		delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();//延时60us
		DQ=1;//拉高
		_nop_();_nop_();//写两个位之间至少有1us的间隔（此处选择2us）
	}
}
uchar read_byte_from_DS18B20()//先读的是低位，整个读周期至少为60us，但控制器采样要在15us内完成，相邻“位”之间至少间隔1us
{
	unsigned char value=0,mask;
	for(mask=0x01;mask!=0;mask<<=1)
	{
		DQ=0;//先把总线拉低超过1us（此处选择2us）后释放
		_nop_();_nop_();
		DQ=1;
		_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();//再延时6us后读总线数据
		if(DQ==0)//如果该位是0
		{
			value&=(~mask);
		}
		else
		{
			value|=mask;
		}
		delay_10us();delay_10us();delay_10us();delay_10us();delay_10us();//再延时52us，凑够至少60us的采样周期
		_nop_();_nop_();
		DQ=1;
		_nop_();_nop_();//写两个位之间至少有1us的间隔（此处选择2us）
	}
	return value;	
}
void get_temperature()
{
	unsigned char Low=0,High=0;
	temperature=0;
	delay_ms(10);//延时10ms度过不稳定期
 
	if(DS18B20_init()==0){
		delay_ms(1);
		write_byte_to_DS18B20(0xcc);//跳过ROM寻址
		write_byte_to_DS18B20(0x44);//启动一次温度转换
		if(first){
			delay_ms(750);//延时1s等待转化
			first=0;
		}
		else{
			delay_ms(20);
		}
	 
		if(DS18B20_init()==0){
			delay_ms(1);
			write_byte_to_DS18B20(0xcc);//跳过ROM寻址
			write_byte_to_DS18B20(0xbe);//发送读值命令·
			Low=read_byte_from_DS18B20();
			High=read_byte_from_DS18B20();
			//获得16位温度值
			temperature=High;
			temperature<<=8;
			temperature|=Low;
			temperature>>=4;
		}
		
	}	
}