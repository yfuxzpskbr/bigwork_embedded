#include "DS18B20.h"
#include "music.h"
#include "delay.h"
#include "LCD1602.h"
#include "timer_clock.h"

void doIt();

void main(){
	uchar der=0;
	LCD_Init();
	init_timer();
	init_INT();
	TR0 = 1;
	Init_Timer1();
	while(1){
		doIt();		
	}		   
}

void doIt(){
	int i;
	SPK = 1;//保护蜂鸣器   ----- 三极管！！
	if(hour==hourC && min==minC && second==secondC && set_clock_mode==0){
		isClock = 1;
		write_str_to_LCD(0,0,"Alarm went off! ");
		write_str_to_LCD(1,0,"Press btn2 close");
	}
	i=0;
	while(isClock){
		if(isClock==0){break;}
		playSong(MUSIC,16);
		delay(100);
		i++;
		if(i==3){isClock=0;break;}
	}
	get_temperature();
	if(set_mode==0){
	show_Time();}
	switch(set_mode){
		case WEEK:
			set_week();
			break;
		case SECOND:
			set_second();
			break;
		case MINUTE:
			set_minute();
			break;
		case HOUR:
			set_hour();
			break;
		case DAY:
			set_day();
			break;
		case MONTH:
			set_month();
			break;
		case YEAR:
			set_year();
			break;
		default:
			TR0=1;
	}
	switch(set_clock_mode){
		case 1:
			set_hourC();
			break;
		case 2:
			set_minuteC();
			break;
		case 3:
			set_secondC();
			break;
	}
}