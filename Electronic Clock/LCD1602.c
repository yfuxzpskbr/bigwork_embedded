#include "LCD1602.h"
#include "delay.h"
void LCD_Init() 
{
	delay_ms(15);
	write_command_to_LCD(0x38);
	delay_ms(5);
	write_command_to_LCD(0x38);
	delay_ms(5);
	write_command_to_LCD(0x38);
	write_command_to_LCD(0x08);
	write_command_to_LCD(0x01);
	write_command_to_LCD(0x06);
	write_command_to_LCD(0x0c);
}

void write_command_to_LCD(uchar data_){
	RS = 0;
	RW = 0;
	E = 0;
	LCD_Buffer=data_;
	_nop_();
	_nop_();
	E = 1;
	_nop_();
	delay_us(100);
	E = 0;
	_nop_();
	_nop_();

}
void write_data_to_LCD(uchar data_){
	RS = 1;
	RW = 0;
	E = 0;
	LCD_Buffer=data_;
	_nop_();
	_nop_();
	E = 1;
	_nop_();
	delay_us(100);
	E = 0;
	_nop_();
	_nop_();
}

void write_str_to_LCD(uchar sx,uchar sy,char *ptr){
	while(*ptr!='\0'){
		if(sx==0)
			write_command_to_LCD(0x80|sy);
		else
			write_command_to_LCD(0x80|0x40|sy);
		write_data_to_LCD(*ptr);
		sy++;
		if(sy==16){
			sx=(sx+1)%2;
			sy=0;
		}
		ptr++;
	}
}